[CmdletBinding()] param ([Switch]$Force=$false)


# Simply copies scripts from ./src to user's bin directory


$ErrorActionPreference = "Stop"
$ScriptFiles = Get-ChildItem -Path .\src -File
$UserBin = "$env:USERPROFILE\bin"
if (-not (Test-Path -Path $UserBin)) {
    New-Item -ItemType Directory -Path $UserBin | Out-Null
}


# Copy script files to the user bin directory
foreach ($ScriptFile in $ScriptFiles) {
    $TargetFile = Join-Path -Path $UserBin -ChildPath $ScriptFile.Name
    
    # Check if the file already exists in the target directory
	if (Test-Path -Path $TargetFile) {
		$FileName = $ScriptFile.Name
        if (-not $Force) {
            Write-Error "Error: '$FileName' already exists in '$UserBin'. Use '-Force' to overwrite."
            exit 1
        }
		Write-Verbose "Overwriting ${TargetFile}..."
    } else {
		Write-Verbose "Creating ${TargetFile}..."
	}
    Copy-Item -Path $ScriptFile.FullName -Destination $UserBin -Force
}

# TODO: Print warning if name exists(and is not a version of a script in this project) on PATH? 
Write-Host "Successfully installed scripts in ${UserBin}"
