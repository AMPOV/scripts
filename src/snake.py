#!/usr/bin/env python


import sys
import re


def validate_arg(arg: str) -> str:
    if arg == (valid := re.match("[A-Za-z ]+", arg).group()):
        return valid
    print("No can do :(")
    exit(1)


arg = validate_arg(sys.argv[1])
arg = f"{arg[0].upper()}{arg[1:]}".strip()
string_sections = [s.strip() for s in re.findall("[A-Z][a-z]*| [a-z]+", arg)]
result_sections = list()
index = 0

while index < len(string_sections):
    if len(string_sections[index]) > 1:
        result_sections.append(string_sections[index])
        index += 1
    else:
        abbr = ""
        while index < len(string_sections) and len(string_sections[index]) == 1:
            abbr += string_sections[index]
            index += 1
        result_sections.append(abbr)
print("_".join(result_sections).lower())
