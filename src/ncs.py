#!/usr/bin/env python


import re
import math
import sys


def ncs_to_rgb(ncs: str, as_hex=False):
    black1, chroma1, red1, factor1, blue1, green1 = 0, 0, 0, 0, 0, 0

    ncs = ncs.upper()
    ncs = ncs.replace("(", "")
    ncs = ncs.replace(")", "")
    ncs = ncs.replace("NCS", "NCS ")
    ncs = re.sub(r"  ", " ", ncs)
    if "NCS" not in ncs:
        ncs = "NCS " + ncs

    match = re.match(r"^(?:NCS|NCS\sS)\s(\d{2})(\d{2})-(N|[A-Z])(\d{2})?([A-Z])?$", ncs)
    if match is None:
        return None

    black = int(match.group(1))
    chroma = int(match.group(2))
    bc = match.group(3)

    if bc not in ["N", "Y", "R", "B", "G"]:
        return None

    percent = int(match.group(4)) if match.group(4) else 0

    if bc != "N":
        black1 = (1.05 * black - 5.25)
        chroma1 = chroma

        if bc == "Y" and percent <= 60:
            red1 = 1
        elif (bc == "Y" and percent > 60) or (bc == "R" and percent <= 80):
            factor1 = percent - 60 if bc == "Y" else percent + 40
            red1 = ((math.sqrt(14884 - factor1 ** 2)) - 22) / 100
        elif (bc == "R" and percent > 80) or (bc == "B"):
            red1 = 0
        elif bc == "G":
            factor1 = percent - 170
            red1 = ((math.sqrt(33800 - factor1 ** 2)) - 70) / 100

        if bc == "Y" and percent <= 80:
            blue1 = 0
        elif (bc == "Y" and percent > 80) or (bc == "R" and percent <= 60):
            factor1 = (percent - 80) + 20.5 if bc == "Y" else (percent + 20) + 20.5
            blue1 = (104 - (math.sqrt(11236 - factor1 ** 2))) / 100
        elif (bc == "R" and percent > 60) or (bc == "B" and percent <= 80):
            factor1 = (percent - 60) - 60 if bc == "R" else (percent + 40) - 60
            blue1 = ((math.sqrt(10000 - factor1 ** 2)) - 10) / 100
        elif (bc == "B" and percent > 80) or (bc == "G" and percent <= 40):
            factor1 = (percent - 80) - 131 if bc == "B" else (percent + 20) - 131
            blue1 = (122 - (math.sqrt(19881 - factor1 ** 2))) / 100
        elif bc == "G" and percent > 40:
            blue1 = 0

        if bc == "Y":
            green1 = (85 - 17 / 20 * percent) / 100
        elif bc == "R" and percent <= 60:
            green1 = 0
        elif bc == "R" and percent > 60:
            factor1 = (percent - 60) + 35
            green1 = (67.5 - (math.sqrt(5776 - factor1 ** 2))) / 100
        elif bc == "B" and percent <= 60:
            factor1 = (1 * percent - 68.5)
            green1 = (6.5 + (math.sqrt(7044.5 - factor1 ** 2))) / 100
        elif (bc == "B" and percent > 60) or (bc == "G" and percent <= 60):
            green1 = 0.9
        elif bc == "G" and percent > 60:
            factor1 = (percent - 60)
            green1 = (90 - (1 / 8 * factor1)) / 100

        factor1 = (red1 + green1 + blue1) / 3
        red2 = ((factor1 - red1) * (100 - chroma1) / 100) + red1
        green2 = ((factor1 - green1) * (100 - chroma1) / 100) + green1
        blue2 = ((factor1 - blue1) * (100 - chroma1) / 100) + blue1

        if red2 > green2 and red2 > blue2:
            max_v = red2
        elif green2 > red2 and green2 > blue2:
            max_v = green2
        elif blue2 > red2 and blue2 > green2:
            max_v = blue2
        else:
            max_v = (red2 + green2 + blue2) / 3

        factor2 = 1 / max_v
        r = int((red2 * factor2 * (100 - black1) / 100) * 255)
        g = int((green2 * factor2 * (100 - black1) / 100) * 255)
        b = int((blue2 * factor2 * (100 - black1) / 100) * 255)

        r = min(255, max(0, r))
        g = min(255, max(0, g))
        b = min(255, max(0, b))
    else:
        grey = int((1 - black / 100) * 255)
        grey = min(255, max(0, grey))
        r, g, b = grey, grey, grey
    
    if as_hex:
        return rgb_to_hex([r, g, b])
    return [r, g, b]


def rgb_to_hex(rgb):
    def clamp(x):
        return max(0, min(255, x))

    r = clamp(rgb[0])
    g = clamp(rgb[1])
    b = clamp(rgb[2])

    return "#{0:02x}{1:02x}{2:02x}".format(r, g, b)


def hex_to_rgb(hex_code):
    if not re.match(r"^#([a-f0-9]{3}){1,2}$", hex_code):
        raise ValueError("Invalid hex code.")

    num = int(hex_code.lstrip("#"), 16)

    red = (num >> 16) & 0xff
    green = (num >> 8) & 0xff
    blue = num & 0xff

    return [red, green, blue]


as_hex = len(sys.argv) > 2 and sys.argv[2] == "--hex"

print(ncs_to_rgb(sys.argv[1], as_hex))
