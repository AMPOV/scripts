#!/usr/bin/env python


import os
import sys
import re
import platform


_CYAN = "\x1b[36m"
_RESET = "\x1b[0m"
separator = ";" if platform.system() == "Windows" else ":"


def colorized(message: str, ansi_sequence: str) -> str:
    return f"{ansi_sequence}{message}{_RESET}"


def get_paths() -> list[str]:
    paths = sorted(os.environ["PATH"].split(separator))
    if len(sys.argv) == 1:
        return paths
    pattern = re.compile(sys.argv[1], flags=re.IGNORECASE)
    return [p for p in paths if pattern.match(p) is not None]


for i, p in enumerate(get_paths()):
    print(colorized(p, _CYAN) if i % 2 == 0 else p)
