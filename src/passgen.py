#!/usr/bin/env python


import itertools
import random
import string
import sys
import argparse
from pathlib import Path
from typing import Any, Iterable


def parse_arguments(args: list[str]) -> dict[str, Any]:
    parser = argparse.ArgumentParser(description="Password generator")
    parser.add_argument("-o", "--output", type=Path, help="Output file path(default: None)")
    parser.add_argument("--length", type=int, default=30, help="Length of generated password(default: 30)")

    # Overrides all other arguments for customizing what characters will be used
    chars_group = parser.add_mutually_exclusive_group()
    chars_group.add_argument("--char-pool",
                             help="Declare all characters that the password may contain as one word. Weight can be added to a character by including it multiple times. Length of pool must be greater than or equal to value of argument lenght. Overrides all other arguments concerning characters used.")
    chars_group.add_argument("--char-set", type=set,
                             help="Declare all distinct characters that the password may contain as one word. Overrides all other arguments concerning characters used.")

    # Is ignored if chars_group argument is passed
    parser.add_argument("--exclude", help="Exclude specific characters. Passed as one word (default: None)")
    parser.add_argument("--no-symbols", "--exclude-symbols", dest="include_symbols", action="store_false",
                        help="Password will not contain any symbols(default: False)")
    parser.add_argument("--no-type-requirements", dest="type_requirements", action="store_false",
                        help="Force inclusion of atleast one character per character type (default: False)")

    return parser.parse_args(args).__dict__


def digits() -> str:
    char_list = [str(i) for i in range(10)]
    return "".join(char_list)


def symbols() -> str:
    char_list = [chr(i) for i in [*range(33, 48), *range(58, 65), 91, *range(93, 97), *range(123, 127)]]
    return "".join(char_list)


def get_char_pools(include_symbols: bool, exclude: str) -> set[str]:
    char_pools = {string.ascii_lowercase, string.ascii_uppercase, digits()}
    if include_symbols:
        char_pools.add(symbols())

    if not exclude:
        return char_pools

    filtered_char_pools = set()
    for cp in char_pools:
        if not (filtered_cp := "".join({c for c in cp if c not in exclude})):
            continue
        filtered_char_pools.add(filtered_cp)

    return filtered_char_pools


def get_mandatory(length: int, char_pools: set[str]) -> dict[int, str]:
    if length < len(char_pools):
        # When number of different character types required exceed length of the password, char_pool is truncated to match length
        char_pools = set(random.sample(sorted(char_pools), k=length))

    indices = random.sample(range(length), len(char_pools))
    mandatory = dict()
    for i, pool in zip(indices, char_pools):
        mandatory[i] = pool[random.randint(0, len(pool) - 1)]
    return mandatory


def generate_sequence(length=30,
                      include_symbols=True,
                      type_requirements=True,
                      exclude: str = None,
                      char_pool: str = None,
                      char_set: str = None) -> Iterable[str]:

    if char_pool:
        return random.sample(population=char_pool, k=length)

    if char_set:
        max_index = len(char_set) - 1
        char_list = list(char_set)
        return [char_list[random.randint(0, max_index)] for _ in range(length)]

    if not (char_pools := get_char_pools(include_symbols, exclude)):
        raise ValueError("All characters excluded")

    # One "word"-pool per character type
    union_char_pool = "".join(itertools.chain(char_pools))

    # print(f"{char_pools=}\n{union_char_pool=}")

    # If type_requirements, populate password with one character per character type at random index
    if type_requirements:
        password = get_mandatory(length, char_pools)
    else:
        password = dict()

    # Fill remaining slots with random characters from union_char_pool
    blank_indices = set(range(length)) - set(password.keys())

    for i in blank_indices:
        password[i] = union_char_pool[random.randint(0, len(union_char_pool) - 1)]

    return password.values()


def main(args: list[str] = None):
    parsed_arguments = parse_arguments(args)
    output = parsed_arguments.pop("output", None)

    if output and output.exists:
        raise FileExistsError

    sequence = list(generate_sequence(**parsed_arguments))
    random.shuffle(sequence)
    password = "".join(sequence)

    if output:
        return output.write_text(password, encoding="utf-8")

    print(password)


if __name__ == "__main__":
    main()
