#!/usr/bin/env python


import os
import sys
from typing import Mapping
import re


_CYAN = "\x1b[36m"
_RESET = "\x1b[0m"


def get_message(i: int, message: str, ansi_sequence: str = _CYAN) -> str:
    if i % 2 == 0:
        return message
    return f"{ansi_sequence}{message}{_RESET}"


def get_variables() -> Mapping[str, str]:
    if len(sys.argv) == 1:
        return os.environ
    pattern = re.compile(sys.argv[1], flags=re.IGNORECASE)
    return {p: v for p, v in os.environ.items() if pattern.match(p) is not None}


WRAP_AT = 88
env = dict(sorted(get_variables().items(), key=lambda i: i[0]))
key_column_width = max(map(len, env.keys())) + 4
indentation = " " * key_column_width


for i, var in enumerate(env.items()):
    value = var[1]
    if len(value) <= WRAP_AT:
        entry = f"{var[0]:<{key_column_width}}{value}"
        print(get_message(i, entry))
        continue

    index = 0
    print(get_message(i, f"{var[0]:<{key_column_width}}{value[index:index + WRAP_AT]}"))
    while index + WRAP_AT < len(value):
        index += WRAP_AT
        print(get_message(i, f"{indentation}{value[index:index + WRAP_AT]}"))
