#!/usr/bin/env python


import base64
import sys
from pathlib import Path
import re


input_path = Path(sys.argv[1])
PREFIX_PATTERN = re.compile(r"^data:image/(\w+?);base64,")


with open(input_path, "r") as f:
    base64_string = f.read()
    

prefix = PREFIX_PATTERN.match(base64_string)
image_format = "png"


if prefix:
    prefix_string = prefix.group()
    image_format = prefix.group(1)
    base64_string = base64_string[len(prefix_string):]
    print(f"Removed prefix '{prefix_string}'")  

print(f"{image_format=}")
output_path = input_path.with_suffix(f".{image_format}")
image_data = base64.b64decode(base64_string)

with open(output_path, "wb") as f:
    f.write(image_data)


print(f"{input_path} -> {output_path}")
