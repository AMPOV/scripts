"""

List all projects and provide functionality to organize by for example language, date of last change etc

"""
import os
from pathlib import Path
from typing import Iterable, Callable, Union
import time
from datetime import timedelta


EXCLUDE_DIRS = {
    ".idea",
    "__pycache__",
    ".git",
    ".settings",
    "target",
    "node_modules",
    "venv",  # Additional venv name-variations can be excluded using 'default_code_dir_exclude'
    ".venv",
    "site-packages",
    ".Trash",
    "Microsoft",
    "Windows"
}


def is_src_file(suffix: str) -> Callable:
    def _is_src_file(p: Path) -> bool:
        return p.suffix == suffix
    return _is_src_file


IS_JAVA_FILE = is_src_file(".java")
IS_PYTHON_FILE = is_src_file(".py")


def src_files(
        path: Path,
        include_file: Callable,
        current_depth=0) -> set[Path]:
    # print(f"recursive({path=}, {include_file=})")

    """
    Enforce basic file-type check in the predicate so that calls like below are not allowed.

    recursive(Path("/some/dir"), include_dir=Path.is_file)

    create subclasses of PathPredicate: FilePredicate and DirPredicate that implicitly check is_file and is_dir

    """

    try:
        current_level = set(filter(include_file, path.iterdir()))
    except PermissionError as e:
        print(f"{path}: {e}")
        return set()

    sub_dirs = [p for p in path.iterdir() if p.is_dir() and ".venv" not in p.name and p.name not in EXCLUDE_DIRS]
    return current_level.union(*[src_files(f, include_file) for f in sub_dirs])


def is_maven_project(project_root: Path) -> bool:
    pom = project_root / Path("pom.xml")
    if not pom.exists():
        return False

    if not (project_root / Path("pom.xml")).exists() or not (project_root / Path("src", "main", "java")).exists():
        return False
    return True


def get_projects_path() -> Path:
    # TODO: Parse args or environment variable fallback
    if (p := os.environ["PROJECTS"]) and (p := Path(p)).exists:
        return p
    raise ValueError("Provide a path to a directory where projects are stored or set environment variable 'PROJECTS'")


def get_project_directories() -> Iterable[Path]:
    return get_projects_path().iterdir()


def since_modification(path: Path) -> int:
    return _since(path.stat().st_mtime)


def since_creation(path: Path) -> int:
    """
    OS-dependent
    """
    try:
        return _since(path.stat().st_birthtime)  # This will most likely fail on linux
    except AttributeError:
        return _since(path.stat().st_ctime)


def _since(timestamp: Union[int | float]) -> int:
    return int(time.time() - timestamp)


def main(args: list[str] = None):

    # maven_projects = list(filter(is_maven_project, get_project_directories()))
    maven_projects = list(filter(Path.is_dir, get_project_directories()))

    # sorted_maven_projects = sorted(maven_projects, key=lambda p: min([since_modification(sp) for sp in src_files(p, IS_JAVA_FILE)]))
    # files_by_dir = {d: files for d in maven_projects if (files := src_files(d, IS_JAVA_FILE))}
    files_by_dir = {d: files for d in maven_projects if (files := src_files(d, lambda f: IS_JAVA_FILE(f) or IS_PYTHON_FILE(f)))}

    since_modification_by_dir = {d: min(map(since_modification, source_files)) for d, source_files in files_by_dir.items()}

    most_recently_modified = dict(sorted(since_modification_by_dir.items(), key=lambda i: i[1]))
    for d, sm in most_recently_modified.items():
        print(f"{d}: {timedelta(seconds=sm)}")

    exit(0)

    for d, source_files in files_by_dir.items():
        most_recent = sorted(source_files, key=since_modification)

        print(p)
        for sf in most_recent[:3]:
            print(f"\t{sf}({since_modification(sf)})")

_DIRECTORY = get_projects_path()


if __name__ == '__main__':
    main()
