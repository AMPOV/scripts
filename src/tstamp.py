#!/usr/bin/env python


import os
import math
import traceback
import itertools
from typing import Iterable, Generator
import sys
from datetime import datetime, UTC


_OUTPUT_FORMAT = "%Y-%m-%dT%H:%M:%S%z"
_DATE_COMPONENTS = "Ymd", "ymd", "mdY", "mdy"
_TIME_COMPONENTS = "HMS",
_TIME_OPTIONS = "", "Z%z", ".%f", ".%fZ%z"
_TIME_SEPARATORS = "", ":"
_DATE_SEPARATORS = "", "-", ".", "/"
_DATE_TIME_SEPARATORS = "", " ", "T"


def to_base_format(format_chars: str):
    # "Ymd" -> "%Y", "%m", "%d", "HMS" -> "%H", "%M", "%S"
    return [f"%{c}" for c in format_chars]


def create_format(base_format: Iterable[str], component_separator: str) -> str:
    # ("%H", "%M", "%S"), ":" -> "%H:%M:%S", ("%H", "%M", "%S"), "" -> "%H%M%S"
    return component_separator.join(base_format)


def to_date_time_format(date_format: str, time_format: str, separator: str) -> str:
    # "%Y%m%d", "%H%M%S.%fZ%z", "T" -> "%Y%m%dT%H%M%S.%fZ%z"
    # print(f"{date_format=}, {time_format=}, {separator=} -> {date_format}{separator}{time_format}")
    return f"{date_format}{separator}{time_format}"


def get_date_formats() -> Iterable[str]:
    base_date_formats = map(to_base_format, _DATE_COMPONENTS)
    return itertools.starmap(create_format, itertools.product(base_date_formats, _DATE_SEPARATORS))


def get_time_formats() -> Iterable[str]:
    base_time_formats = map(to_base_format, _TIME_COMPONENTS)
    time_formats = itertools.starmap(create_format, itertools.product(base_time_formats, _TIME_SEPARATORS))
    pairs = itertools.product(time_formats, _TIME_OPTIONS)
    return ("".join(p) for p in pairs)


def get_date_time_formats() -> Iterable[str]:
    return itertools.starmap(to_date_time_format, itertools.product(get_date_formats(), get_time_formats(), _DATE_TIME_SEPARATORS))


def get_formats() -> list[str]:
    return sorted(itertools.chain(get_date_time_formats(), get_date_formats()), key=len, reverse=True)


def try_parse(value: str, dtf: str) -> datetime:
    try:
        return datetime.strptime(value, dtf)
    except ValueError:
        pass


def parse_datetime(dt: str) -> datetime | None:
    for dtf in get_formats():
        if (result := try_parse(dt, dtf)) is not None:
            return result


def to_datetime(ts: str) -> datetime | None:
    if not (int_part := ts.split(".")[0]).isnumeric():
        return

    length = len(int_part)
    if length == 10:
        return datetime.fromtimestamp(int(int_part))
    if length == 13:
        return datetime.fromtimestamp(int(int_part) / 1000)


def to_timestamp(dt: datetime) -> int:
    return int(dt.timestamp() * 1000)


def list_formats():
    dt = datetime.now(UTC)
    formats = get_formats()
    for f in formats:
        print(f"{f:<24} -> {dt.strftime(f)}")
    print(f"\nNumber of formats recognized: {len(formats)}")


def parse_argument() -> str | None:
    arg_count = len(sys.argv) - 1
    if arg_count == 0:
        return
    if arg_count == 1:
        return sys.argv[1]

    # Handles calls that pass date and time as two separate arguments
    return "".join(sys.argv[1:])


def main(argument: str = None):
    if not argument:
        dt = datetime.now()
        return f"{to_timestamp(dt)}, {dt.strftime(_OUTPUT_FORMAT)}"

    if argument == "--formats":
        list_formats()
        exit(0)

    if result := to_datetime(argument):
        return result.strftime(_OUTPUT_FORMAT)

    if result := parse_datetime(argument):
        return to_timestamp(result)
    return f"Could not find determine temporal format for argument: {argument}"


if __name__ == '__main__':
    print(main(parse_argument()))
