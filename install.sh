#!/bin/bash


# Simply copies scripts from ./src to user's bin directory


# Exit on error
set -e

SCRIPT_FILES=$(ls ./src)
USER_BIN=~/bin
mkdir -p "$USER_BIN"

# Check for -f or --force argument
FORCE=false
for arg in "$@"; do
    if [[ "$arg" == "-f" || "$arg" == "--force" ]]; then
        FORCE=true
        break
    fi
done


# Copy script files to ~/bin
for F in $SCRIPT_FILES; do
    
	# Skipping directories
    if [[ ! -f "./src/$F" ]]; then
	    continue
	fi
	
	TARGET_NAME=$(basename "$F" .${F##*.})
	TARGET_FILE="${USER_BIN}/${TARGET_NAME}"  # Strip the extension
	
	# Check if the file already exists in the target directory
    if [[ -e "$TARGET_FILE" ]]; then
        if [[ "$FORCE" == false ]]; then
            echo "Error: '$TARGET_FILE' already exists. Use '-f' or '--force' to overwrite."
            exit 1
        fi
		echo "Overwriting ${TARGET_FILE}..."
    else
	    echo "Creating ${TARGET_FILE}..."
	fi
	
	# TODO: Print warning if name conflict($TARGET_FILE exists in PATH and points to a file that is not a version of a script in this project)?
	cp "./src/$F" "$TARGET_FILE"
	chmod +x $TARGET_FILE


done
echo "Successfully installed scripts in ${USER_BIN}"
